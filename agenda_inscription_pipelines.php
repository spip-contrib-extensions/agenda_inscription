<?php
/**
 * Utilisations de pipelines par Inscription Agenda
 *
 * @plugin     Inscription Agenda
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Agenda_inscription\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}




/**
 * Charger des styles CSS dans l'espace privé
 *
 * @pipeline insert_head_css
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
**/
function agenda_inscription_header_prive_css($flux) {
	$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('css/agenda_inscription.css')) . '" type="text/css" />' . "\n";
	return $flux;
}




/**
 * Ajouter les objets sur les vues des parents directs
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function agenda_inscription_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec']) and $e['edition'] == false) {
		$id_objet = $flux['args']['id_objet'];

		if ($e['type'] == 'evenement') {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/evenements_inscrits',
				[
					'titre' => _T('evenements_inscrit:titre_evenements_inscrits'),
					'id_evenement' => $id_objet,
				], 
				['ajax' => true]
			);

			// ajouter un nouvel inscrit
			if (autoriser('creerevenementsinscritdans', 'evenements', $id_objet)) {
				// uniquement si l'evenement a les inscriptions ouvertes et est non finies
				if ($row = sql_fetsel('inscription,places', 'spip_evenements', 'id_evenement='.intval($id_objet).' AND inscription > 0 AND date_fin>'.sql_quote(date('Y-m-d H:i:s')))) {
					include_spip('inc/presentation');
					$flux['data'] .= icone_verticale(
						_T('evenements_inscrit:icone_creer_evenements_inscrit'),
						generer_url_ecrire('evenements_inscrit_edit', "id_evenement=$id_objet"),
						'evenements_inscrit-24.png',
						'new',
						'right'
					) . "<br class='nettoyeur' />";
				}
			}
		}
	}
	return $flux;
}

/**
 * Afficher le nombre d'éléments dans les parents
 *
 * @pipeline boite_infos
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function agenda_inscription_boite_infos($flux) {
	if (isset($flux['args']['type']) and isset($flux['args']['id']) and $id = intval($flux['args']['id'])) {
		$texte = '';
		if ($flux['args']['type'] == 'evenement' and $nb = sql_countsel('spip_evenements_inscrits', array("statut='publie'", 'id_evenement=' . $id))) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'evenements_inscrit:info_1_evenements_inscrit', 'evenements_inscrit:info_nb_evenements_inscrits') . "</div>\n";
		}
		if ($texte and $p = strpos($flux['data'], '<!--nb_elements-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		}
	}
	return $flux;
}


/**
 * Compter les enfants d'un objet
 *
 * @pipeline objets_compte_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function agenda_inscription_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'evenement' and $id_evenement = intval($flux['args']['id_objet'])) {
		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['evenements_inscrits'] = sql_countsel('spip_evenements_inscrits', 'id_evenement= ' . intval($id_evenement) . " AND (statut = 'publie')");
		} else {
			$flux['data']['evenements_inscrits'] = sql_countsel('spip_evenements_inscrits', 'id_evenement= ' . intval($id_evenement) . " AND (statut <> 'poubelle')");
		}
	}

	return $flux;
}

/**
* Pipeline de la corbeille, permet de définir les objets à supprimer
*
* @param array $param Tableau d'objets
*
* @return array Tableau d'objets complété
*/
function agenda_inscription_corbeille_table_infos($param){
	$param['evenements_inscrits'] = array(
		'statut' => 'poubelle',
		'table'=> 'spip_evenements_inscrits',
		'tableliee'=> array(),
	);
	return $param;
}


/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function agenda_inscription_optimiser_base_disparus($flux) {

	sql_delete('spip_evenements_inscrits', "statut='poubelle' AND maj < " . sql_quote(trim($flux['args']['date'],"'")));

	return $flux;
}
