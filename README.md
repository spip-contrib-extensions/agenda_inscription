Inscription libre aux événements
===========

![agenda_inscription](./prive/themes/spip/images/agenda_inscription-xx.svg)

Propose un module alternatif aux inscriptions des événements de l’agenda



Documentation:\
https://contrib.spip.net/Agenda-Inscription-libre-aux-evenements