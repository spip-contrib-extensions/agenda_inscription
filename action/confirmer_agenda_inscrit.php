<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Confirmer l'inscription d'un email a un evenement (inscription deja en base)
 *
 * @param string $arg
 */
function action_confirmer_agenda_inscrit_dist($arg = null) {

	if ($arg = _request('arg')) {

		list($id_evenement, $alea, $email) = explode("-", $arg);

		// prendre des infos sur l'événements
		if (!$row = sql_fetsel('*', 'spip_evenements', 'id_evenement='.intval($id_evenement))) {
			// evenement n'existe  pas
			include_spip('inc/minipres');
			echo minipres();
			exit;
		}
		$titre_evt = $row['titre'];
		$date_debut = $row['date_debut'];

		// verifier que l'alea existe
		if (!$row2 = sql_fetsel('*', 'spip_evenements_inscrits', 'id_evenement='.intval($id_evenement). ' AND alea = '.sql_quote($alea))) {
			// alea inconnu
			include_spip('inc/minipres');
			echo minipres(_T('agenda_inscription:action_inscription_alea_invalide', array('titre' => $titre_evt, 'email' => $email)));
			exit;
		}
		$id_evenements_inscrit = $row2['id_evenements_inscrit'];

		$statut = 'publie';

		// si les places sont comptees, regarder s'il en reste
		$is_inscription_normale = true;
		if ($places = $row['places']) {
			$nb_ok = sql_countsel('spip_evenements_inscrits', 'id_evenement='.intval($id_evenement)." AND statut='publie'");
			if ($nb_ok >= $places) {
				$is_liste_attente = implode("", lire_config('agenda_inscription/liste_attente', array()));
				if ($is_liste_attente) {
					// evenement complet  - inscription sur liste d'attente
					$is_inscription_normale = false;
					$statut = 'refuse';
				} else {
					// evenement complet sans liste d'attente on arrete la
					include_spip('inc/minipres');
					echo minipres(_T('agenda_inscription:action_inscription_complet', array('titre' => $titre_evt, 'email' => $email)));
					exit;
				}
			}
		}

		// on est arrivée jusque là .... on peut confirmer l'inscription !
		sql_updateq('spip_evenements_inscrits', array('statut' => $statut, 'alea' => ''), 'id_evenement ='.intval($id_evenement).' AND alea=' . sql_quote($alea));

		// envoyer une confirmation email à l'usager
		$envoyer_mail = charger_fonction('envoyer_mail', 'inc/');
		$destinataire_email = $email;
		$email_html = recuperer_fond('emails/agenda_inscrit_confirme', 	array(
			'email' => $email,
			'id_evenement' => $id_evenement,
			'is_inscription_normale' => $is_inscription_normale
		));

		include_spip('inc/filtres_dates');
		$date_debut = sql_getfetsel('date_debut', 'spip_evenements',  "id_evenement=" . intval($id_evenement));
		$date_debut_str = affdate_base($date_debut," (d/m/Y)");

		if ($is_inscription_normale) {
			$email_client_sujet = _T('agenda_inscription:email_confirme_titre')." : ".$titre_evt.$date_debut_str;
		} else {
			$email_client_sujet = _T('agenda_inscription:email_confirme_titre_liste_attente')." : ".$titre_evt.$date_debut_str;
		}
		$email_client_corps = array(
			'html' => $email_html,
		);

		$ok = $envoyer_mail($destinataire_email, $email_client_sujet, $email_client_corps);

		// notification aux admins si l'option est activée
		if ($destinataires_notif = lire_config('agenda_inscription/destinataires', 0)) {
			$destinataires_notif = array_map('trim', explode(",", $destinataires_notif));

			$email_html = recuperer_fond('emails/agenda_inscrit_confirme_notification', array(
				'email' => $email,
				'id_evenement' => $id_evenement,
				'id_evenements_inscrit' => $id_evenements_inscrit,
				'is_inscription_normale' => $is_inscription_normale,
			));

			if ($is_inscription_normale) {
				$email_client_sujet = _T('agenda_inscription:email_confirme_notif_titre')." : ".$titre_evt.$date_debut_str;
			} else {
				$email_client_sujet = _T('agenda_inscription:email_confirme_notif_titre_liste_attente')." : ".$titre_evt.$date_debut_str;
			}

			$email_client_corps = array(
				'html' => $email_html,
			);

			$ok = $envoyer_mail($destinataires_notif, $email_client_sujet, $email_client_corps);

		}


		// affichage final
		include_spip('inc/minipres');
		if ($is_inscription_normale) {
			echo minipres(_T('agenda_inscription:action_inscription_confirmee', array('titre' => $titre_evt, 'email' => $email)));
		} else {
			echo minipres(_T('agenda_inscription:action_inscription_confirmee_liste_attente', array('titre' => $titre_evt, 'email' => $email)));
		}
		exit;

	} else {
		// pas argument  : erreur
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}

}
