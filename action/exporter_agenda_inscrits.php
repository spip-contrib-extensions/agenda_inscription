<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Action pour exporter les inscrits en csv / xls
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_exporter_agenda_inscrits_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	list($id_evenement,$filtre) = explode("-", $arg);

 	if ($id_evenement) {
		exporter_csv_agenda_inscrits($id_evenement, $filtre);
	} else {
		spip_log("action_agenda_inscrits_dist $arg pas compris");
	}
}


function exporter_csv_agenda_inscrits($id_evenement, $filtre) {

	$requete_sql_where = array(
		'id_evenement = '. intval($id_evenement)
	);

	if ($filtre == 'ok') {
		$requete_sql_where[] = 'statut = "publie"';
	}



	$inscrits = array();
	$entetes = array(
		'nom' => _T('evenements_inscrit:champ_nom_label'),
		'prenom' => _T('evenements_inscrit:champ_prenom_label'),
		'email' => _T('evenements_inscrit:champ_email_label'),
		'tel' => _T('evenements_inscrit:champ_tel_label'),
		'fonction' => _T('evenements_inscrit:champ_fonction_label'),
		'statut' => _T('evenements_inscrit:champ_statut_label'),
		'date' => _T('evenements_inscrit:champ_date_label'),
		'notes' => _T('evenements_inscrit:champ_notes_label'),
	);

	if ($resultats = sql_select('*','spip_evenements_inscrits',	$requete_sql_where)) {

		while ($ligne = sql_fetch($resultats)) {
			$statut = $ligne['statut'];
			if ($statut == "publie") {
				$statut = "OK";
			}
			if ($statut == "refuse") {
				$statut = "Liste d'attente";
			}
			$inscrits[] = array(
					'nom' => $ligne['nom'],
					'prenom' => $ligne['prenom'],
					'email' => $ligne['email'],
					'tel' => $ligne['tel'],
					'fonction' => $ligne['fonction'],
					'statut' => $statut,
					'date' => $ligne['date'],
					'notes' => $ligne['notes'],
			);
		}

		// export csv
		include_spip('base/exporter_csv');  // spip_bonux
		$exporter_csv = charger_fonction('exporter_csv','inc');
		$delim = _request('delim')?_request('delim'):"TAB";

		// titre fichier
		$titre_evenement = sql_getfetsel('titre', 'spip_evenements',  "id_evenement=" . intval($id_evenement));
		/*
		include_spip('inc/filtres_dates');
		$date_debut = sql_getfetsel('date_debut', 'spip_evenements',  "id_evenement=" . intval($id_evenement));
		$date_debut_str = affdate_base($date_debut,"Ymd");
		$nom_export = 'inscrits_' . $filtre . "_" . $date_debut_str . "-" .$titre_evenement;
		*/

		$nom_export = 'inscrits_' . $filtre . "-" .$titre_evenement;


		exporter_csv_agenda_inscrit($nom_export, $inscrits, $delim, $entetes);

	}

}



// duplicata de la fonction de spip bonux
// sur le serveur de prod, l'export de passe pas
// - transmission du fichier se fait pas variable et non par fichier (bug serveur prod)
// - on export sous csv --> ajouter transcode
function exporter_csv_agenda_inscrit($titre, $resource, $delim=',', $entetes = null,$envoyer = true){

	$filename = preg_replace(',[^-_\w]+,', '_', translitteration(textebrut(typo($titre))));

	if ($delim == 'TAB') $delim = "\t";
	if (!in_array($delim,array(',',';',"\t")))
		$delim = ",";

	$charset = $GLOBALS['meta']['charset'];
	$importer_charset = null;
	if ($delim == ',') {
		$extension = 'csv';
		$importer_charset = $charset = 'iso-8859-1';  // ajout pour excel trop bete (erational)
	} else {
		$extension = 'xls';
		# Excel n'accepte pas l'utf-8 ni les entites html... on transcode tout ce qu'on peut
		$importer_charset = $charset = 'iso-8859-1';
	}
	$filename = $filename."_export_".date("Ymd").".".$extension;
	$output = "";

	if ($entetes AND is_array($entetes) AND count($entetes))
		$output .= exporter_csv_ligne($entetes,$delim,$importer_charset);


	while ($row=is_array($resource)?array_shift($resource):sql_fetch($resource)){
		$output .= exporter_csv_ligne($row,$delim,$importer_charset);
	}


	// mise en place de l'entete pour envoyer le fichier csv
	if ($envoyer) {
		header("Content-Type: application/csv-tab-delimited-table");
		header("Content-disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		echo($output);  // hack pour sur le serveur de prod, pb de transmission de fichier tmp/cache/export
		die();
	}

	return $fichier;
}
