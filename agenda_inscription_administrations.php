<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Inscription Agenda
 *
 * @plugin     Inscription Agenda
 * @copyright  2020
 * @author     erational@erational.org
 * @licence    GNU/GPL
 * @package    SPIP\Agenda_inscription\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Inscription Agenda.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function agenda_inscription_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	$maj['create'] = array(array('maj_tables', array('spip_evenements_inscrits')));

	// Ajout champs "fonction"
	$maj['1.1.0'] = array(
		array('sql_alter', "TABLE spip_evenements_inscrits ADD `fonction`  tinytext DEFAULT '' NOT NULL AFTER `tel`")
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Inscription Agenda.
 * 
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function agenda_inscription_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_evenements_inscrits');

	# Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', array('evenements_inscrit')));
	sql_delete('spip_mots_liens', sql_in('objet', array('evenements_inscrit')));
	sql_delete('spip_auteurs_liens', sql_in('objet', array('evenements_inscrit')));
	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', array('evenements_inscrit')));
	sql_delete('spip_versions_fragments', sql_in('objet', array('evenements_inscrit')));
	sql_delete('spip_forum', sql_in('objet', array('evenements_inscrit')));

	effacer_meta($nom_meta_base_version);
}
