<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Inscription Agenda
 * @copyright  2020-2021
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Agenda_inscription\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function agenda_inscription_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['evenements_inscrits'] = 'evenements_inscrits';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function agenda_inscription_declarer_tables_objets_sql($tables) {

	$tables['spip_evenements_inscrits'] = array(
		'type' => 'evenements_inscrit',
		'principale' => 'oui',
		'table_objet_surnoms' => array('evenementsinscrit'), // table_objet('evenements_inscrit') => 'evenements_inscrits' 
		'field'=> array(
			'id_evenements_inscrit' => 'bigint(21) NOT NULL',
			'id_evenement'       => 'bigint(21) NOT NULL DEFAULT 0',
			'nom'                => 'text NOT NULL DEFAULT ""',
			'prenom'             => 'text NOT NULL DEFAULT ""',
			'email'              => 'tinytext NOT NULL DEFAULT ""',
			'tel'                => 'tinytext NOT NULL DEFAULT ""',
			'fonction'           => 'tinytext NOT NULL DEFAULT ""',
			'notes'              => 'text NOT NULL DEFAULT ""',
			'alea'               => 'tinytext NOT NULL DEFAULT ""',
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_evenements_inscrit',
			'KEY id_evenement'   => 'id_evenement',
			'KEY statut'         => 'statut',
		),
		'titre' => 'nom AS titre, "" AS lang',
		'date' => 'date',
		'page' => false, // pas de page publique
		'champs_editables'  => array('nom', 'prenom', 'email', 'tel', 'fonction','notes', 'id_evenement'),
		'champs_versionnes' => array('nom', 'prenom', 'email', 'tel', 'fonction', 'notes', 'id_evenement'),
		'rechercher_champs' => array("nom" => 2),
		'tables_jointures'  => array(),
		'statut_titres' => array(
			'prop' => 'evenements_inscrit:info_inscrit_propose',
			'publie' => 'evenements_inscrit:info_inscrit_publie',
			'poubelle' => 'evenements_inscrit:info_inscrit_supprime'
		),
		'statut_textes_instituer' => array(
			// 'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'evenements_inscrit:texte_statut_propose',
			'publie'   => 'evenements_inscrit:texte_statut_publie',
			'refuse'   => 'evenements_inscrit:texte_statut_refuse',
			'poubelle' => 'evenements_inscrit:texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'evenements_inscrit:texte_changer_statut_evenements_inscrit',


	);

	return $tables;
}
