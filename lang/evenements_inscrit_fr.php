<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// A
	'ajouter_lien_evenements_inscrit' => 'Ajouter cet inscrit',

	// B
	'bilan_inscription_active' => 'Inscription en ligne',
	'bilan_inscription_places_a_confirmer' => 'Inscriptions en attente de confirmation',
	'bilan_inscription_places_confirmees' => 'Inscriptions confirmées',
	'bilan_inscription_attente_confirme' => 'Personnes sur liste d\'attente',
	'bouton_inscrire' => 'S\'inscrire à l\'événement',

	// C
	'champ_alea_label' => 'Alea',
	'champ_email_label' => 'Email',
	'champ_nom_label' => 'Nom',
	'champ_notes_label' => 'Notes',
	'champ_prenom_label' => 'Prénom',
	'champ_tel_label' => 'Téléphone',
	'champ_fonction_label' => 'Fonction',
	'champ_date_label' => 'Date d\'inscription',
	'champ_statut_label' => 'Statut',
	'confirmer_supprimer_evenements_inscrit' => 'Confirmez-vous la suppression de cet inscrit ?',

	// I
	'icone_creer_evenements_inscrit' => 'Inscrire une nouvelle personne',
	'icone_modifier_evenements_inscrit' => 'Modifier cet inscrit',
	'info_1_evenements_inscrit' => 'Un inscrit',
	'info_aucun_evenements_inscrit' => 'Aucun inscrit à cet événement',
	'info_evenements_inscrits_auteur' => 'Les inscrits de cet auteur',
	'info_nb_evenements_inscrits' => '@nb@ inscrits',
	'info_inscrit_propose' => 'Inscription à confirmer',
	'info_inscrit_publie'  => 'Inscription confirmée',
	'info_inscrit_supprime'  => 'A la poubelle',


	// R
	'retirer_lien_evenements_inscrit' => 'Retirer cet inscrit',
	'retirer_tous_liens_evenements_inscrits' => 'Retirer tous les inscrits',

	// S
	'supprimer_evenements_inscrit' => 'Supprimer cet inscrit',
	'statut_inscription' => 'Statut de l\'inscription',

	// T
	'texte_ajouter_evenements_inscrit' => 'Inscrire une nouvelle personne',
	'texte_changer_statut_evenements_inscrit' => 'Cette inscription est :',
	'texte_creer_associer_evenements_inscrit' => 'Créer et associer un inscrit',
	'texte_definir_comme_traduction_evenements_inscrit' => 'Ce inscrit est une traduction du inscrit numéro :',
	'texte_statut_propose' => 'A confirmer',
	'texte_statut_publie'  => 'Confirmée',
	'texte_statut_refuse'  => 'Liste d\'attente',
	'texte_statut_poubelle'  => 'A la poubelle',
	'telecharger_oui' => 'Télécharger les inscriptions confirmées',
	'telecharger_toutes' => 'Télécharger toutes les inscriptions',
	'titre_evenement' => 'Événement',
	'titre_evenements_inscrit' => 'Inscrit',
	'titre_evenements_inscrits' => 'Inscrits',
	'titre_evenements_inscrits_rubrique' => 'Inscrits de la rubrique',
	'titre_langue_evenements_inscrit' => 'Langue de cet inscrit',
	'titre_logo_evenements_inscrit' => 'Logo de cet inscrit',
	'titre_objets_lies_evenements_inscrit' => 'Liés à cet inscrit',
	'titre_formulaire' => 'S\'inscrire à l\'événement',
];
