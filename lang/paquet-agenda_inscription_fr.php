<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// A
	'agenda_inscription_description' => 'Propose une méthode alternative pour s\'inscrire aux événements de l\'agenda. Les inscriptions sont ouvertes à tou(te)s et sont à confimer par email.',
	'agenda_inscription_nom' => 'Inscription libre aux événements',
	'agenda_inscription_slogan' => 'Un autre type d\'inscription pour les événements de l\'agenda',
];
