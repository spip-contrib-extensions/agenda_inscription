<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

// functions
function get_inscription_champs() {
	return array('nom', 'prenom', 'email', 'tel', 'fonction', 'nobot');
}

function get_inscription_champs_obligatoires() {
	return array('nom', 'prenom', 'email');
}

// Charger
function formulaires_inscrire_evenement_charger_dist($id_evenement) {
	$champs = get_inscription_champs();
	$valeurs = array();
	foreach($champs as $champ) {
		$valeurs[$champ]='';
	}

	// si pas d'evenement ou d'inscription, on echoue silencieusement
	if (!$row = sql_fetsel('inscription,places', 'spip_evenements', 'id_evenement='.intval($id_evenement).' AND date_fin>'.sql_quote(date('Y-m-d H:i:s')))
		or !$row['inscription']) {
		return false;
	}

	// valeurs d'initialisation
	$valeurs['id_evenement'] = $id_evenement;


	// si les places sont comptees, regarder s'il en reste
	if ($places = $row['places']) {
		$nb_ok = sql_countsel('spip_evenements_inscrits', 'id_evenement='.intval($id_evenement)." AND statut='publie'");

		if ($nb_ok >= $places) {
 			$is_liste_attente = implode("", lire_config('agenda_inscription/liste_attente', array()));
			if ($is_liste_attente) {
				// evenement complet - liste attente
				$valeurs['message_ok'] = _T('agenda_inscription:evenement_complet_liste_attente');
			} else {
				// evenement complet
				$valeurs['editable'] = false;
				$valeurs['message_ok'] = _T('agenda_inscription:evenement_complet');
			}
		}
	}

	return $valeurs;
}

// Vérifier
function formulaires_inscrire_evenement_verifier_dist($id_evenement) {

	$erreurs = array();
	$champs_obligatoires = get_inscription_champs_obligatoires();

	// verifier les champs obligatoires
	foreach($champs_obligatoires as $obligatoire) {
		if (!_request($obligatoire)) {
			$erreurs[$obligatoire] = _T('info_obligatoire');
		}
	}

	// pas de double inscription avec le même email
	$email = strtolower(trim(_request('email')));
	if ($email !='' AND sql_fetsel('nom', 'spip_evenements_inscrits', 'id_evenement='.intval($id_evenement).' AND email='.sql_quote($email))) {
		$erreurs['email'] = _T('erreur_email_deja_existant');
	} else {
		// email valide ?
		include_spip('inc/filtres');
		if (!email_valide($email)) {
			$erreurs['email'] = _T('form_email_non_valide');;
		}
	}

	if (_request('nobot')) {
		$erreurs['message_erreur'] = _T('pass_rien_a_faire_ici');
	}

	return $erreurs;
}


// Traiter
function formulaires_inscrire_evenement_traiter_dist($id_evenement) {

	// recuperer les champs
	$champs =get_inscription_champs();
	foreach ($champs as $k=>$champ) {
		if (_request($champ)) {
			$$champ =  trim(strip_tags(_request($champ)));
		} else {
			$$champ = "";
		}
	}

	// champs forcee
	$date = date('Y-m-d H:i:s');
	$alea = md5($nom.time());
	$statut = "prop";

	// determiner s'il s'agit d'une inscription normale ou sur liste d'attente
	$is_inscription_normale = true;
	$row = sql_fetsel('places', 'spip_evenements', 'id_evenement='.intval($id_evenement));
	// mode evenement à places limitees
	if ($places = $row['places']) {
		$nb_ok = sql_countsel('spip_evenements_inscrits', 'id_evenement='.intval($id_evenement)." AND statut='publie'");
		if ($nb_ok >= $places) {
 			$is_liste_attente = implode("", lire_config('agenda_inscription/liste_attente', array()));
			if ($is_liste_attente) {
				// il s'agit d'une inscription sur liste attente
				$is_inscription_normale = false;
			} else {
				// erreur:  on arrete la.
				return array('message_erreur'=>_T('agenda_inscription:evenement_complet'), 'editable'=>false);
			}
		}
	}

	// enregistrement reponse
	$requete_sql = array(
		'id_evenement' => $id_evenement,
		'nom' => $nom,
		'prenom' => $prenom,
		'email' => $email,
		'tel' => $tel,
		'fonction' => $fonction,
		'email' => $email,
		'date' => $date,
		'alea' => $alea,
		'statut' => $statut,
	);
	$id_evenement_inscrit = sql_insertq('spip_evenements_inscrits', $requete_sql);

	// envoi email avec alea
	$envoyer_mail = charger_fonction('envoyer_mail', 'inc/');

	$destinataire_email = $email;
	$email_html = recuperer_fond('emails/agenda_inscrit_confirmation', 	array(
		'email' => $email,
		'id_evenement' => $id_evenement,
		'alea' => $alea,
		'is_inscription_normale' => $is_inscription_normale,
	));
	$titre_evenement = sql_getfetsel('titre', 'spip_evenements',  "id_evenement=" . intval($id_evenement));

	include_spip('inc/filtres_dates');
	$date_debut = sql_getfetsel('date_debut', 'spip_evenements',  "id_evenement=" . intval($id_evenement));
	$date_debut_str = affdate_base($date_debut," (d/m/Y)");

	if ($is_inscription_normale) {
		$email_client_sujet = _T('agenda_inscription:email_confirmation_titre')." : ".$titre_evenement.$date_debut_str;
	} else {
		$email_client_sujet = _T('agenda_inscription:email_confirmation_titre_attente')." : ".$titre_evenement.$date_debut_str;
	}
	$email_client_corps = array(
		'html' => $email_html,
	);

	// adresse envoi personnalisée ?
	$adresse_envoi_nom_perso = lire_config('agenda_inscription/adresse_envoi_nom', false);
	$adresse_envoi_email_perso = lire_config('agenda_inscription/adresse_envoi_email', false);
	if ($adresse_envoi_nom_perso && $adresse_envoi_email_perso) {
		$email_client_corps['nom_envoyeur'] = $adresse_envoi_nom_perso;
		$email_client_corps['from'] = $adresse_envoi_email_perso;
	}

	$ok = $envoyer_mail($destinataire_email, $email_client_sujet, $email_client_corps);

	include_spip('inc/invalideur');
	suivre_invalideur("id='evenement/$id_evenement'");

	if ($is_inscription_normale) {
		$message = _T('agenda_inscription:envoi_email_confirmation', array('email' => "<b>$email</b>"));
	} else {
		$message = _T('agenda_inscription:envoi_email_confirmation_attente', array('email' => "<b>$email</b>"));
	}

	return array('message_ok'=>$message, 'editable'=>false);
}
