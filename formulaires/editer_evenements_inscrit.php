<?php
/**
 * Gestion du formulaire de d'édition de evenements_inscrit
 *
 * @plugin     Inscription Agenda
 * @copyright  2020
 * @author     erational@erational.org
 * @licence    GNU/GPL
 * @package    SPIP\Agenda_inscription\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');


/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_evenements_inscrit
 *     Identifiant du evenements_inscrit. 'new' pour un nouveau evenements_inscrit.
 * @param int $id_evenement
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenements_inscrit source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du evenements_inscrit, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_evenements_inscrit_identifier_dist($id_evenements_inscrit = 'new', $id_evenement = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_evenements_inscrit)));
}

/**
 * Chargement du formulaire d'édition de evenements_inscrit
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_evenements_inscrit
 *     Identifiant du evenements_inscrit. 'new' pour un nouveau evenements_inscrit.
 * @param int $id_evenement
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenements_inscrit source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du evenements_inscrit, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_evenements_inscrit_charger_dist($id_evenements_inscrit = 'new', $id_evenement = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('evenements_inscrit', $id_evenements_inscrit, $id_evenement, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_evenement']) {
		$valeurs['id_evenement'] = $id_evenement;
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de evenements_inscrit
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_evenements_inscrit
 *     Identifiant du evenements_inscrit. 'new' pour un nouveau evenements_inscrit.
 * @param int $id_evenement
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenements_inscrit source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du evenements_inscrit, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_evenements_inscrit_verifier_dist($id_evenements_inscrit = 'new', $id_evenement = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$erreurs = array();

	$erreurs = formulaires_editer_objet_verifier('evenements_inscrit', $id_evenements_inscrit, array('nom', 'id_evenement'));

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de evenements_inscrit
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_evenements_inscrit
 *     Identifiant du evenements_inscrit. 'new' pour un nouveau evenements_inscrit.
 * @param int $id_evenement
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenements_inscrit source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du evenements_inscrit, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_evenements_inscrit_traiter_dist($id_evenements_inscrit = 'new', $id_evenement = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('evenements_inscrit', $id_evenements_inscrit, $id_evenement, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
